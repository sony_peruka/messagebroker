package com.web.pervacio.test;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.log4j.Logger;

public class MessageBrokerTestHelper {

	private static Logger logger = Logger.getLogger(MessageBrokerTestHelper.class);
	public static String postData(String gUIPUTDATA, String imei, String type) {
		// TODO Auto-generated method stub
		return callService(gUIPUTDATA, imei,type);
	}

	public static String getData(String iMEI, String type, int ackid) {
		// TODO Auto-generated method stub
		return callGetService(iMEI,type,ackid);
	}
	
	
	private static String callService(String data, String imei, String type){
		
		String serviceurl = "http://localhost:8080/bootstrap/putData?imei="+ imei +"&type="+type+"&data="+data;
		String output =null;
		try {
			URL url = new URL(serviceurl);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");
			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : "
						+ conn.getResponseCode());
			}
			BufferedReader br1 = new BufferedReader(new InputStreamReader(
					(conn.getInputStream())));
			
			while ((output = br1.readLine()) != null) {
				logger.info(output);
			}
			conn.disconnect();

		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return output;
	}

private static String callGetService(String imei, String type, int ackid){
		
		String serviceurl = "http://localhost:8080/bootstrap/getData?imei="+ imei +"&type="+type+"&ackid="+ackid;
		String output =null;
		try {
			URL url = new URL(serviceurl);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");
			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : "
						+ conn.getResponseCode());
			}
			BufferedReader br1 = new BufferedReader(new InputStreamReader(
					(conn.getInputStream())));
			
			while ((output = br1.readLine()) != null) {
				logger.info(output);
			}
			conn.disconnect();

		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return output;
	}
	
}
