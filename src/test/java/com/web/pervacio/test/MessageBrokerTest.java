/*package com.web.pervacio.test;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;

import com.pervacio.eternity.diag.PDCommand;
import com.web.pervacio.app.util.DiagUtil;

public class MessageBrokerTest{

	private static String GUIPUTDATA = null;
	private static String DEVICEPUTDATA = null;
	
	private static String GUIGETDATA = null;
	private static String DEVICEGETDATA = null;
	
	private static String IMEI1 = "123querty";
	private static String IMEI2 = "987asdfg";
	
	
	
	@BeforeClass	
	public static void setUp() throws Exception {
		
		PDCommand pdc = new PDCommand();
		pdc.setCmdId(0);
		pdc.setDcmd("WiFiTest");
		pdc.setData("PASS");
		GUIPUTDATA = pdc.toString();
		
		pdc = new PDCommand();
		pdc.setCmdId(0);
		pdc.setDcmd("LEDTest");
		pdc.setData("FAIL");
		DEVICEPUTDATA = pdc.toString();
	}
	
	boolean isGUIPosted,isDevicePosted = false;

	@Test
	public void testAdd() {
		String status = MessageBrokerTestHelper.postData(GUIPUTDATA,IMEI1,"gui");
		PDCommand pdCommand=DiagUtil.fromJson(status, PDCommand.class);
		
		if(pdCommand.getData().equalsIgnoreCase("SUCCESS")){
			isGUIPosted =true;
			String response = MessageBrokerTestHelper.getData(IMEI1,"device",0);
			logger.info(response);
			assertEquals(response, GUIPUTDATA );
		}
		//assertEquals(sum, total);
	}
	
	public void devicePostData() {
		String status = MessageBrokerTestHelper.postData(DEVICEPUTDATA,IMEI2,"device");
		PDCommand pdCommand = DiagUtil.fromJson(status, PDCommand.class);
		// assertNotSame(sum, total);
		if (pdCommand.getData().equalsIgnoreCase("SUCCESS")) {
			isDevicePosted = true;
			String response =  MessageBrokerTestHelper.getData(IMEI2,null,0);
			assertEquals(GUIPUTDATA, response);
		}

	}
	
	
	
	
}
*/