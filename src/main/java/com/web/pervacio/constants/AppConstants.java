package com.web.pervacio.constants;


public interface AppConstants {
	public static final String CONFIG_FILE = "/DMServer/care/app-config.properties";
	public static final String FACADE_LOCAL="local";
	public static final String FACADE_REMOTE="remote";
	public static final String OTA_SERVICE_URL="server.url";
	public static final String DEVICE_URL = "device.url";
	public static final String PLATFORM_ANDROID = "Android";
	public static final String PLATFORM_IOS = "ios";
	public String DATE_TIMEZONE="date.timezone";
	public static final String PATH_DEVICE_CONFIG_FILE = "path.deviceConfigFile";
	public static final String PATH_DIAG_CONFIG_FILE = "path.diagConfigFile";
		
}
