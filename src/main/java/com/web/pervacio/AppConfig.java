package com.web.pervacio;

import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.env.Environment;

import com.web.pervacio.app.MessageBrokerFacade;
import com.web.pervacio.app.MessageBrokerFacadeLocal;

@ComponentScan
@EnableAutoConfiguration
@Configuration
@PropertySources({ @PropertySource("classpath:config.properties") /*,@PropertySource(value = "file:" + AppConstants.CONFIG_FILE, ignoreResourceNotFound = true)*/ 
})
public class AppConfig extends SpringBootServletInitializer{

	private static AppConfig appConfig;
	
	public static void main(String[] args) {
		SpringApplication.run(AppConfig.class, args);
    }
	
	public AppConfig() {
		appConfig = this;
	}
	
	@Autowired
	private Environment env;

	@Bean
	public static PropertySourcesPlaceholderConfigurer propertyPlaceHolderConfigurer() {
		return new PropertySourcesPlaceholderConfigurer();
	}

	/*@Bean
	public JavaMailSenderImpl javaMailSenderImpl() {
		final JavaMailSenderImpl mailSenderImpl = new JavaMailSenderImpl();
		mailSenderImpl.setHost(env.getProperty("smtp.host"));
		mailSenderImpl.setPort(env.getProperty("smtp.port", Integer.class));
		mailSenderImpl.setProtocol(env.getProperty("smtp.protocol"));
		mailSenderImpl.setUsername(env.getProperty("smtp.username"));
		mailSenderImpl.setPassword(env.getProperty("smtp.password"));
		final Properties javaMailProps = new Properties();
		javaMailProps.put("mail.smtp.auth", true);
		javaMailProps.put("mail.smtp.starttls.enable", true);
		javaMailProps.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		javaMailProps.put("mail.smtp.starttls.enable", true);
		javaMailProps.put("mail.smtp.ssl.trust", "smtp.gmail.com");
		mailSenderImpl.setJavaMailProperties(javaMailProps);
		return mailSenderImpl;
	}*/

	public static String getProperty(String key) {
		return appConfig.env.getProperty(key);
	}

	public static <T> T getProperty(String key, Class<T> type) {
		return appConfig.env.getProperty(key, type);
	}

	@Bean
	public MessageBrokerFacade getFacade() {
		/*String facadeType = env.getProperty("facade.service");
		if(facadeType.equalsIgnoreCase(AppConstants.FACADE_LOCAL)){
			return new MessageBrokerFacadeLocal();
		}else if(facadeType.equalsIgnoreCase(AppConstants.FACADE_REMOTE)){
			return new MessageBrokerFacadeRemote();
		}*/
		return new MessageBrokerFacadeLocal();
	}
	
	@Bean
	public static String updateLog4j(){
		LogManager.getRootLogger().setLevel(Level.toLevel(AppConfig.getProperty("log4j.rootCategory")));
		return "";
	}
}