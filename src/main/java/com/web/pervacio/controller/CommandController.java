package com.web.pervacio.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.web.pervacio.app.MessageBrokerFacade;
import com.web.pervacio.app.util.PayLoadData;

@RestController
@RequestMapping("/bootstrap")
public class CommandController {
	
	@Autowired
	private MessageBrokerFacade messageBrokerFacade;
	
	@RequestMapping(value="/putData",produces = "application/json")
	public String putData(
			@RequestParam(value="data",required=false) String data,
			@RequestParam(value="type",required=false) String type,
			@RequestParam(value="deviceid",required=false) String deviceid){
			
			if(null == data){
				return PayLoadData.sendJsonResponse("DATA_IS_REQUIRED");
			}
			if("gui".equalsIgnoreCase(type)){
				if(null==deviceid){
					return PayLoadData.sendJsonResponse("DEVICEID_IS_REQUIRED");
				}
				return messageBrokerFacade.postData(deviceid,data,type);
			}else{
				return messageBrokerFacade.postData(deviceid,data,type);
			}
	}
	
	@RequestMapping(value="/getData",produces = "application/json")
	public String getData(
			@RequestParam(value="type",required=false) String type,
			@RequestParam(value="tout",required=false) Integer tout,
			@RequestParam(value="ackid",required=false) Integer ackid,
			@RequestParam(value="deviceid",required=false) String deviceid){

			if(null==deviceid && (type==null || type=="device")){
				return PayLoadData.sendJsonResponse("DEVICEID_IS_REQUIRED");
			}
			if(null==ackid){
				return PayLoadData.sendJsonResponse("ACKID_IS_REQUIRED");
			}
			if(null!=tout){
				return messageBrokerFacade.retrieveData(deviceid, type!=null?type:"device",tout, ackid);
			}else{
				return messageBrokerFacade.retrieveData(deviceid, type!=null?type:"device", ackid);	
			}
			
	}
}