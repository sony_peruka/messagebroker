package com.web.pervacio.app.util;

import com.google.gson.Gson;

public class PayLoadData {

	private String DeviceId;
	private String EventName;
	private String Status;
	
	public PayLoadData(){}
	
	public PayLoadData(String deviceId, String eventName, String status) {
		super();
		DeviceId = deviceId;
		EventName = eventName;
		Status = status;
	}
	
	public String getDeviceId() {
		return DeviceId;
	}
	public void setDeviceId(String deviceId) {
		DeviceId = deviceId;
	}
	public String getEventName() {
		return EventName;
	}
	public void setEventName(String eventName) {
		EventName = eventName;
	}
	public String getStatus() {
		return Status;
	}
	public void setStatus(String status) {
		Status = status;
	}

	public String toJson() {
		Gson gson = new Gson();
		return gson.toJson(this);
	}
	
	public static String sendJsonResponse(String str){
		Gson gson = new Gson();
		PayLoadData payLoadData = new PayLoadData();
		payLoadData.setEventName(str);
		return gson.toJson(payLoadData);
	}
	
	
}
