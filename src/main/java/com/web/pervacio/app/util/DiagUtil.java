package com.web.pervacio.app.util;

import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.lang.reflect.Type;
//import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;
import java.util.zip.GZIPInputStream;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.web.pervacio.AppConfig;
import com.web.pervacio.constants.AppConstants;

public class DiagUtil {

	private static final Logger logger = Logger.getLogger(DiagUtil.class);
	
	public static final String CMD_RESPONSE = "CMD_RESPONSE";
	private static DateFormat dateFormatter=new SimpleDateFormat("dd/MM/yyyy HH:mm");
	public static final Map<String,String> testNames=new HashMap<String,String>();
	public static final Map<String,String> issueNames=new HashMap<String,String>();
	public DiagUtil() {
	}
	
	
	public static String getProperMakeName(String make) {
		if ("samsung".equalsIgnoreCase(make)) {
			return "Samsung";
		} else if ("lge".equalsIgnoreCase(make)) {
			return "LG";
		} else if ("htc".equalsIgnoreCase(make)) {
			return "HTC";
		} else if ("TCT".equalsIgnoreCase(make)) {
			return "Alcatel";
		}
		return make;
	}

	public static <T> T fromJson(String data, Class<T> type) {
		try {
			Gson gson = new Gson();
			return gson.fromJson(data, type);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		return null;
	}

	public static <T> T fromJson(String data, Type type) {
		try {
			Gson gson = new Gson();
			return gson.fromJson(data, type);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		return null;
	}

	public static String toJson(Object data) {
		if(data instanceof String) {
			return (String)data;
		}
		try {
			Gson gson = new Gson();
			return gson.toJson((data));
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		return null;
	}

	public static String sendResponse(String str){
		String response = "{\"resposne\" :"+str+"}";
		logger.info("response: "+response);
		return response;
	}
	/*public static String getResult(String cmd, String data) {
		PDCommand pdCommand = new PDCommand();
		pdCommand.setDcmd(cmd);
		pdCommand.setData(data);
		return toJson(pdCommand);
	}

	
	public static String shortCommandData(String data){		
    	if(data!=null && !logger.getEffectiveLevel().equals(Level.DEBUG)){
    		PDCommand pdCommand=fromJson(data, PDCommand.class);
    		if(pdCommand.getData()!=null&&pdCommand.getData().length()>100){
    			data=pdCommand.getDcmd()+":"+pdCommand.getData().substring(0, 99);
    		}else{
    			data=pdCommand.getDcmd()+":"+pdCommand.getData();
    		}
    	}
    	return data;
    }*/
	
	public static String ungzip(byte[] bytes) {
		if (bytes != null) {
			try {
				logger.info("data size before:"+bytes.length);
				InputStreamReader isr = new InputStreamReader(new GZIPInputStream(new ByteArrayInputStream(bytes)),"UTF_8");
				StringWriter sw = new StringWriter();
				char[] chars = new char[1024];
				for (int len; (len = isr.read(chars)) > 0;) {
					sw.write(chars, 0, len);
				}
				String data=sw.toString();
				logger.info("data size after:"+data.length());
				return data;
			}catch(Exception e){
				logger.error(e.getMessage(), e);
			}
		}
		return null;
	}
	
	
	public static String formatDate(Date date){
		if(AppConfig.getProperty("Date.formatter")!=null){
			dateFormatter=new SimpleDateFormat(AppConfig.getProperty("Date.formatter"));
		}
		dateFormatter.setTimeZone(TimeZone.getTimeZone(AppConfig.getProperty(AppConstants.DATE_TIMEZONE)));
		return dateFormatter.format(date);
	}	
}