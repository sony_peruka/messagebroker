package com.web.pervacio.app;

public interface MessageBrokerFacade {

	public static final int DEFAULT_WAIT_TIME_IN_SEC = 30;

	public String postData(String imei, String data, String type);
	
	public String retrieveData(String imei, String type, Integer ackid);
	
	public String retrieveData(String imei, String type, Integer tout, Integer ackid);

}
