package com.web.pervacio.app;

import java.util.Calendar;
import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;

import com.web.pervacio.app.util.PayLoadData;

public class MessageBrokerFacadeLocal implements MessageBrokerFacade {

	private static final Logger logger = Logger.getLogger(MessageBrokerFacadeLocal.class);
	
	private static final String SUCCESS = "SUCCESS";
	private static final String FAIL = "FAIL";
	private static final String TIMEOUT = "TIMEOUT";
	private static final String NO_QUEUE_CREATED = "NO_QUEUE_CREATED";
	private static final long CLEANUP_TIME = 10 * 60 * 1000;
	private int guiAckId = 0;
	private String guiPrvStr = null;
	
	private BlockingQueue<String> guiQueue = new ArrayBlockingQueue<String>(50); 
	private static final Map<String, DeviceSession> queueMap = new ConcurrentHashMap<String, DeviceSession>();
	
	private class DeviceSession {
		String imei;
		BlockingQueue<String> deviceQueue = new ArrayBlockingQueue<String>(50);
		Calendar lastUpdatedTime = Calendar.getInstance();
		String prevCommand;
		int deviceAckId =0;
		DeviceSession(String imei) {
			this.imei = imei;
		}
	}
	
	@PostConstruct
	public void monitorAndCleanOldQueues() {
		logger.info("monitor imeiSessions thread started....");
		(new Thread() {
			@Override
			public void run() {
				while (true) {
					try {
						Thread.sleep(CLEANUP_TIME);
					} catch (Exception e) {
					}
					cleanUnusedQueues();
				}
			}
		}).start();
	}

	private void cleanUnusedQueues() {
		logger.info("Cleanup thread started");
		Collection<DeviceSession> deviceSessionData = queueMap.values();
		logger.info("liveSessions size: "+deviceSessionData.size());
		for (DeviceSession deviceSession : deviceSessionData) {
			if (System.currentTimeMillis() - deviceSession.lastUpdatedTime.getTimeInMillis() >= CLEANUP_TIME) {
				DeviceSession session = queueMap.remove(deviceSession.imei);
				logger.info("End Session imei:" + session.imei);
			}
		}
	}

	@Override
	public String postData(String imei, String data, String type) {
		// TODO Auto-generated method stub
		String result = null;
		boolean status = false;
		BlockingQueue<String> dataQ = null;
		try {
			if ("gui".equalsIgnoreCase(type)) {
				logger.info("GUI Posting data: " + data);
				if (queueMap.containsKey(imei)) {
					DeviceSession deviceSession = queueMap.get(imei);
					dataQ = deviceSession.deviceQueue;
					deviceSession.lastUpdatedTime = Calendar.getInstance();
					status = dataQ.offer(data);
				} else {
					DeviceSession deviceSession = new DeviceSession(imei);
					dataQ = deviceSession.deviceQueue;
					deviceSession.lastUpdatedTime = Calendar.getInstance();
					queueMap.put(imei, deviceSession);
					status = dataQ.offer(data);
				}
			} else {
				logger.info("Device Posting data: " + data);
				status = guiQueue.offer(data);
			}
			if (status) {
				result = PayLoadData.sendJsonResponse(SUCCESS);
			} else {
				result = PayLoadData.sendJsonResponse(FAIL);
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		logger.info("result: " + result);
		return result;
	}
	
	@Override
	public String retrieveData(String imei, String type, Integer ackid) {
		// TODO Auto-generated method stub
		return this.retrieveData(imei,type, DEFAULT_WAIT_TIME_IN_SEC, ackid);
	}
	
	@Override
	public String retrieveData(String imei, String type,
			Integer defaultWaitTimeInSec, Integer ackid) {
		// TODO Auto-generated method stub
		String result = null;
		DeviceSession deviceSession = null;
		try {
			if ("gui".equalsIgnoreCase(type)) {

				if (guiAckId > ackid) {
					logger.info("[GUI] Sent Previous data to GUI: " + imei
							+ ",data: " + guiPrvStr + " and ackid: " + ackid);
					return guiPrvStr;
				}
				result = guiQueue.poll(defaultWaitTimeInSec, TimeUnit.SECONDS);
				if (null == result) {
					return PayLoadData.sendJsonResponse(TIMEOUT);
				} else {
					guiAckId = ackid;
					guiPrvStr = result;
					logger.info("[GUI] GUI Pulling, data: " + result + " and ackid: "
							+ ackid);
					return result;
				}
			} else {
				if (queueMap.containsKey(imei)) {
					deviceSession = queueMap.get(imei);
					if (null != deviceSession) {
						if(ackid == 0){
							deviceSession.deviceAckId =0;
						}
						if (deviceSession.deviceAckId > ackid) {
							result = deviceSession.prevCommand;
							logger.info("[DEVICE] Sent Previous data to device: " + imei
									+ ",data: " + result + " and ackid: "
									+ ackid);
							return result;
						}
						result = deviceSession.deviceQueue.poll(defaultWaitTimeInSec,
								TimeUnit.SECONDS);

						if (null == result) {
							return PayLoadData.sendJsonResponse(TIMEOUT);
						} else {
							deviceSession.prevCommand = result;
							deviceSession.deviceAckId = ackid;
							logger.info("[DEVICE] Sending data to device,data: "
									+ result + ",imei: " + imei
									+ " and ackid: " + ackid);
							return result;
						}
					}
				} else {
					logger.info("[DEVICE] NO Queue Created.");
					return PayLoadData.sendJsonResponse(NO_QUEUE_CREATED);
				}
			}
			/*result = dataQ.poll(defaultWaitTimeInSec, TimeUnit.SECONDS);
			
			if (null == result) {
				return PayLoadData.sendJsonResponse(TIMEOUT);
			} else {
				if ("gui".equalsIgnoreCase(type)) {
					guiAckId = ackid;
					guiPrvStr = result;
					logger.info("Sending data to gui, data: "+result+" and ackid: "+ackid);
				} else {
					deviceSession.prevCommand = result;
					deviceAckId = ackid;
					logger.info("Sending data to device,data: " +result +",imei: "+imei+" and ackid: "+ackid);
				}
			}*/
		}catch(Exception e){
			logger.error(e.getMessage(), e);
		}
		return result;
	}
}
